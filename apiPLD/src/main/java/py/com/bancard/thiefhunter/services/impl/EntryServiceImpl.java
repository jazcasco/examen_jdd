package py.com.bancard.thiefhunter.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import py.com.bancard.thiefhunter.mappers.EntryMapper;
import py.com.bancard.thiefhunter.models.Entry;
import py.com.bancard.thiefhunter.services.EntryService;
import py.com.bancard.thiefhunter.utils.DataPaginationDTO;
import py.com.bancard.thiefhunter.utils.PaginationDTO;

/**
 * @author jcasco
 */
@Service
public class EntryServiceImpl implements EntryService {

	@Autowired
	private EntryMapper entryMapper;

	@Override
	public DataPaginationDTO<Entry> matchName(String name, String lastname, Integer limit, Integer offset) {
		List<Entry> matches = entryMapper.findByFuzzyName(name.toUpperCase(), lastname.toUpperCase(), limit, offset);
		Integer total = entryMapper.count(name, lastname);

		DataPaginationDTO<Entry> pagination = new DataPaginationDTO<>();
		pagination.setData(matches);
		pagination.setStatus(new PaginationDTO(offset, limit, total));

		return pagination;
	}
}
