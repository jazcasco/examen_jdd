package py.com.bancard.thiefhunter.services;

import py.com.bancard.thiefhunter.models.Entry;
import py.com.bancard.thiefhunter.utils.DataPaginationDTO;

/**
 * @author jcasco
 */
public interface EntryService {

	DataPaginationDTO<Entry> matchName(String name, String lastname,Integer limit, Integer offset);
}