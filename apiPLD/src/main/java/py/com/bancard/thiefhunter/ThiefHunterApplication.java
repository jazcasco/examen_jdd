package py.com.bancard.thiefhunter;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableAsync
@EnableScheduling
@MapperScan("py.com.bancard.thiefhunter.mappers")
@PropertySource("classpath:application.properties")
public class ThiefHunterApplication {

	public static void main(String[] args) {
		SpringApplication.run(ThiefHunterApplication.class, args);
	}

}
