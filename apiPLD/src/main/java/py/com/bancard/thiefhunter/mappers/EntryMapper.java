package py.com.bancard.thiefhunter.mappers;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import py.com.bancard.thiefhunter.models.Entry;

/**
 * @author jcasco
 */
@Mapper
public interface EntryMapper {

	@Select(value = "select * from entries where UPPER(firstname) % #{name} " + " or UPPER(secondname) % #{name} "
			+ " or UPPER(thirdname) % #{name} " + " or UPPER(lastname) % #{lastname} limit #{limit} offset #{offset}")
	List<Entry> findByFuzzyName(@Param("name") String name, @Param("lastname") String lastname,
			@Param("limit") Integer limit, @Param("offset") Integer offset);


	@Select(value = "select count(*) from entries where UPPER(firstname) % #{name} "
			+ " or UPPER(secondname) % #{name} " + " or UPPER(thirdname) % #{name} "
			+ " or UPPER(lastname) % #{lastname}")
	Integer count(@Param("name") String name, @Param("lastname") String lastname);
}
