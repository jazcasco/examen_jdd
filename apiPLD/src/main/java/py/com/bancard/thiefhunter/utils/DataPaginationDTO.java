package py.com.bancard.thiefhunter.utils;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DataPaginationDTO<T> {
	@JsonProperty(value = "data")
	private List<T> data;

	@JsonProperty(value = "status")
	private PaginationDTO status;

	public List<T> getData() {
		return data;
	}

	public void setData(List<T> data) {
		this.data = data;
	}

	public PaginationDTO getStatus() {
		return status;
	}

	public void setStatus(PaginationDTO status) {
		this.status = status;
	}
}