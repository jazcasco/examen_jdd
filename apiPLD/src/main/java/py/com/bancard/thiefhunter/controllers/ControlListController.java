package py.com.bancard.thiefhunter.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import py.com.bancard.thiefhunter.models.Entry;
import py.com.bancard.thiefhunter.services.EntryService;
import py.com.bancard.thiefhunter.utils.DataPaginationDTO;

/**
 * @author jcasco
 */
@Controller
@RequestMapping("control-list")
public class ControlListController {

	public static final Logger LOGGER = LoggerFactory.getLogger(ControlListController.class);

	@Autowired
	private EntryService entryService;

	@GetMapping(value = "search")
	public ResponseEntity<DataPaginationDTO<Entry>> match(@RequestParam(name = "name", required = true) String name,
			@RequestParam(name = "lastname", required = true) String lastname,
			@RequestParam(value = "page", required = false, defaultValue = "0") Integer page,
			@RequestParam(value = "page_size", required = false, defaultValue = "10") Integer pageSize) {

		return ResponseEntity.ok(entryService.matchName(name, lastname, pageSize, page));

	}
}
