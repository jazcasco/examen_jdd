package com.mybatis.example.demo.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DataPaginationDTO<T> {
	@JsonProperty(value = "data")
	private List<T> data;

	@JsonProperty(value = "status")
	private PaginationDTO status;

	public DataPaginationDTO() {
		super();
	}

	public DataPaginationDTO(List<T> data, PaginationDTO status) {
		super();
		this.data = data;
		this.status = status;
	}

	public List<T> getData() {
		return data;
	}

	public void setData(List<T> data) {
		this.data = data;
	}

	public PaginationDTO getStatus() {
		return status;
	}

	public void setStatus(PaginationDTO status) {
		this.status = status;
	}
}