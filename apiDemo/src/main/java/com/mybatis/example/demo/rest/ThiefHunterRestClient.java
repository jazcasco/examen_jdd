package com.mybatis.example.demo.rest;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.mybatis.example.demo.dto.BaseResponse;
import com.mybatis.example.demo.dto.DataPaginationDTO;

/**
@author jcasco
*/
@Component
public class ThiefHunterRestClient {

	@Value("${thief-hunter.rest.url}")
	private String thiefHunterBaseURL;
	
	private HttpHeaders buildHeader() {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
		return headers;
	}
	
	public DataPaginationDTO search(String name, String lastname) {

		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = buildHeader();
		HttpEntity<String> entity = new HttpEntity<String>(headers);
		String q = "?name="+name.toUpperCase()+"&lastname="+lastname.toUpperCase();
		
		ResponseEntity<DataPaginationDTO> response = restTemplate.exchange(
				thiefHunterBaseURL + "/control-list/search"+q,
				HttpMethod.GET, entity, DataPaginationDTO.class);

		return response.getBody();
	}
	
}
