package com.mybatis.example.demo.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mybatis.example.demo.dto.DataPaginationDTO;
import com.mybatis.example.demo.dto.EntryDTO;
import com.mybatis.example.demo.dto.PaginationDTO;
import com.mybatis.example.demo.dto.PeopleIdentification;
import com.mybatis.example.demo.mapper.PeopleIdentificationMapper;
import com.mybatis.example.demo.rest.ThiefHunterRestClient;

@RestController
public class ProvisioningController {

	@Autowired
	private ThiefHunterRestClient restClient;

	@Autowired
	private PeopleIdentificationMapper mapper;

	@RequestMapping(value = "/api")
	public ResponseEntity<DataPaginationDTO<EntryDTO>> provision(
			@RequestParam(name = "name", required = true) String name,
			@RequestParam(name = "lastname", required = true) String lastname) {
		return ResponseEntity.ok(restClient.search(name, lastname));
	}

	@RequestMapping(value = "/api/{id}")
	public ResponseEntity<DataPaginationDTO<EntryDTO>> provision1(@PathVariable String id) {
		PeopleIdentification pId = mapper.findById(id);
		if (pId != null)
			return ResponseEntity.ok(restClient.search(pId.getFirstName(), pId.getLastName()));
		else
			return ResponseEntity.ok(new DataPaginationDTO<>(new ArrayList<>(), new PaginationDTO()));

	}
}