package com.mybatis.example.demo.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Select;

import com.mybatis.example.demo.dto.PeopleIdentification;

/**
 * @author jcasco
 */
public interface PeopleIdentificationMapper {
	@Select("SELECT first_name as firstName, last_name as lastName  "
			+ " FROM profile.people_identification WHERE id = #{id}")
	PeopleIdentification findById(String id);

	@Select("SELECT id, type, birth_date, last_name, first_name, full_name, status, occupation_id, city_id, last_change_date, pdv_id, verified, creation_date, ofac, saf, expiration_date "
			+ " FROM profile.people_identification")
	List<PeopleIdentification> findAll();
}
