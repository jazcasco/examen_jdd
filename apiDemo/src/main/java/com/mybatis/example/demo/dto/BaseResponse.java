package com.mybatis.example.demo.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * @author jcasco
 */
@JsonInclude(value = Include.NON_NULL)
public class BaseResponse {

	private Boolean success;
	private String description;
	private Object data;

	public BaseResponse() {
		super();
	}

	public BaseResponse(Boolean success, String description) {
		super();
		this.success = success;
		this.description = description;
	}

	public BaseResponse(Boolean success, Object data) {
		super();
		this.success = success;
		this.data = data;
	}

	public BaseResponse(Boolean success, String description, Object data) {
		super();
		this.success = success;
		this.description = description;
		this.data = data;
	}

	public Boolean getSuccess() {
		return success;
	}

	public void setSuccess(Boolean success) {
		this.success = success;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

}
