package com.mybatis.example.demo.dto;

import java.util.Date;

/**
 * @author jcasco
 */
public class PeopleIdentification {

	private String id;
	private String fullName;
	private String firstName;
	private String lastName;
	private Date birthDate;
	private String status;
	private String occupation;
	private String residence;
	private Date lastChangeDate;
	private Long pdv;
	private Boolean verified;
	private String saf;

	public PeopleIdentification() {
		super();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getLastChangeDate() {
		return lastChangeDate;
	}

	public void setLastChangeDate(Date lastChangeDate) {
		this.lastChangeDate = lastChangeDate;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getOccupation() {
		return occupation;
	}

	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}

	public String getResidence() {
		return residence;
	}

	public void setResidence(String residence) {
		this.residence = residence;
	}

	public Long getPdv() {
		return pdv;
	}

	public void setPdv(Long pdv) {
		this.pdv = pdv;
	}

	public Boolean getVerified() {
		return verified;
	}

	public void setVerified(Boolean verified) {
		this.verified = verified;
	}

	public String getSaf() {
		return saf;
	}

	public void setSaf(String saf) {
		this.saf = saf;
	}

}
